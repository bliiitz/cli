#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='bliiitz CLI tool',  
    version='1.1.0',
    scripts=[
        'bliiitz/bliiitz'
    ],
    author="Benjamin Briet",
    author_email="benjamin.briet@protonmail.com",
    description="Bliiitz tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bliiitz/cli",
    packages=setuptools.find_packages("bliiitz"),
    install_requires=[
        'click',
        'python-gitlab',
        'GitPython',
        'boto3',
        'halo',
        'pycrypto',
        'Jinja2'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)