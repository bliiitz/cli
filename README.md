# Bliiitz Command Line Interface

## Description
`bliiitz` is a tools for manage secrets of Devops/Cloud tools account. The
secrets store is stored encrypted by a master password on a git repository backend. 
Tools implemented: Gitlab, AWS, Kubernetes

## Requirement

The installation of the package need `python3` and `pip` package manager 

## Installation

Install the latest package with pip:
```shell
python3 -m pip install --upgrade git+https://gitlab.com/bliiitz/cli.git@1.0.0 --user
```

## Contribute
You can ask access to the repository on this [gitlab repository](https://gitlab.com/bliiitz/cli).
Merge requests for new integration or bug fixing are welcome :)

## Initialize

In first time, you need to create a private git repository for the secrets backend.
You can create it on your favorite git SCM manager (gihtub, gitlab, etc...).
If data is already present on git repository, it will be imported.

You need to setup git credentials in environment vars:
```shell
export GIT_URL=https://gitlab.com
export GIT_USER=username
export GIT_PASSWORD=Your_PERSONNAL_TOKEN
```

You can set this vars in CLI too:
```shell
bliiitz init --git-url https://gitlab.com --git-user username --git-password Your_PERSONNAL_TOKEN
```

For every others command, the CLI ask you for the master password to encrypt/decrypt git repository content.
You can store the master password in your home directory with:
```shell
bliiitz secrets store-master-password
``` 

## Features

### Remember master password
```
Usage: bliiitz secrets store-master-password [OPTIONS]

  Store master password localy

Options:
  --help  Show this message and exit.
```

### Gitlab Secrets

You can store multiple Gitlab accounts in the secrets store.
```
Usage: bliiitz secrets gitlab [OPTIONS] COMMAND [ARGS]...

  Gitlab secrets functions

Options:
  --help  Show this message and exit.

Commands:
  add   Add Gitlab account
  list  list Gitlab account stored
  rm    Remove Gitlab account
  show  Display data of Gitlab account
```

### Gitlab functions
#### bliiitz gitlab sync
```
Usage: bliiitz gitlab sync [OPTIONS] PATH

  Clone a project or clone recursively a Gitlab namespace

Options:
  --name TEXT  Gitlab account name
  --recursive  Take a group path and clone all projects recursively in current
               dir
  --help       Show this message and exit.
```

### AWS secrets
You can store multiple AWS accounts in the secrets store.
```
Usage: bliiitz secrets aws [OPTIONS] COMMAND [ARGS]...

  AWS secrets functions

Options:
  --help  Show this message and exit.

Commands:
  add   Add aws access/secret key
  list  list aws accounts stored
  rm    Remove aws access/secret key
  show  Show aws access/secret key
```

### AWS functions
#### bliiitz aws generate
```
Usage: bliiitz aws generate [OPTIONS]

  Generate aws credentials files ( ~/.aws/config & ~/.aws/credentials )

Options:
  --help  Show this message and exit.
``` 

#### bliiitz aws rotate
```
Usage: bliiitz aws rotate [OPTIONS]

  Rotate aws access/secret key

Options:
  --all        Rotate all AWS keys
  --name TEXT  Name of AWS account's key to rotate
  --help       Show this message and exit.
```

#### bliiitz aws reset-password
```
Usage: bliiitz aws reset-password [OPTIONS]

  Reset your AWS console password

Options:
  --name TEXT      AWS account name  [required]
  --password TEXT  New password  [required]
  --help           Show this message and exit.
```

### Kubernetes secrets
You can store multiple kubeconfig in the secrets store.
```
Usage: bliiitz secrets kube [OPTIONS] COMMAND [ARGS]...

  Kubernetes secrets functions

Options:
  --help  Show this message and exit.

Commands:
  add   Add kubeconfig
  list  list kubeconfig stored
  rm    Remove kubeconfig
  show  Display kubeconfig
```

### Kubernetes functions
#### bliiitz kube setup
You can store multiple kubeconfig in the secrets store.
```
Usage: bliiitz kube setup [OPTIONS]

  Setup a kubeconfig on ~/.kube/config

Options:
  --name TEXT  kubeconfig name to setup  [required]
  --help       Show this message and exit.
```

